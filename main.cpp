﻿#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
using namespace std::chrono_literals;

int main()
{
    sf::RenderWindow window(sf::VideoMode(1000, 800), L"Фигурки)");
    sf::CircleShape circle(100.f);
    circle.setFillColor(sf::Color::Green);
    circle.setOrigin(100, 100);
    int circle_x = 900;
    int circle_y = 100;
    circle.setPosition(circle_x, circle_y);
    sf::CircleShape triangle(80, 3);
    triangle.setFillColor(sf::Color::Red);
    triangle.setOrigin(40, 40);
    int triangle_x = 777;
    int triangle_y = 300;
    triangle.setPosition(triangle_x, triangle_y);
    sf::CircleShape hexagon(200, 6);
    hexagon.setFillColor(sf::Color::Blue);
    hexagon.setOrigin(100, 100);
    int hexagon_x = 669;
    int hexagon_y = 505;
    hexagon.setPosition(hexagon_x, hexagon_y);
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        circle_x-=3;
        if (circle_x < 100)
            circle_x = 100;
        circle.setPosition(circle_x, circle_y);

        triangle_x -= 2.1;
        if (triangle_x < 30)
            triangle_x = 30;
        triangle.setPosition(triangle_x, triangle_y);

        hexagon_x -= 1.2;
        if (hexagon_x < 80)
            hexagon_x = 80;
        hexagon.setPosition(hexagon_x, hexagon_y);
        
        std::this_thread::sleep_for(1ms);
        window.clear();
        window.draw(circle);
        window.draw(triangle);
        window.draw(hexagon);
        window.display();
    }
    

    return 0;
}